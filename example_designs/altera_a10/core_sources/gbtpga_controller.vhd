-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

-- Libraries for direct instantiation:
library frmclk_pll;
library alt_sv_issp_gbtBank1;
library alt_sv_issp_gbtBank2;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity gbtfpga_controller is   
   port (   
      
      --===============--
      -- Clock and Reset --
      --===============--      
     
      CLK_i                                             			: in  std_logic; 
      RST_i                                             			: in  std_logic; 
    
      --================--
      -- AvMM interface --
      --================--
      
      AvMM_Addr      														: in  std_logic_vector(9 downto 0); 
		
      AVMM_Read          													: in  std_logic;
      AvMM_ReadData      													: out std_logic_vector(31 downto 0); 
	   AvMM_ReadDataValid													: out std_logic;
		
	   AvMM_Write         													: in  std_logic;
	   AvMM_WriteData															: in  std_logic_vector(31 downto 0);
		
      --==============--
      -- SCA          --
      --==============--
		reset_gbtfpga															: out std_logic;
		gbtRxReady																: in  std_logic
   );
end gbtfpga_controller;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of gbtfpga_controller is

	signal local_addr				: std_logic_Vector(7 downto 0);
	
begin
	
							
	registers : process(RST_i, CLK_i)
	begin
			-- Reset Init
			if RST_i = '1'	then				
				
				AvMM_ReadData      		<= (others => '0');
				AvMM_ReadDataValid 		<= '0';
				
				
			elsif CLK_i'event and CLK_i ='1' then
				
								
				AvMM_ReadDataValid	<= '0'; 
				
				
				if AVMM_Read = '1' then
				
					case AvMM_Addr(9 downto 2) is
					
						when x"00"	=>	AvMM_ReadData 			<= x"0000000" & "000" & gbtRxReady;
											AvMM_ReadDataValid	<= '1';
					
						when others => AvMM_ReadData			<= x"bad0bad0";
											AvMM_ReadDataValid	<= '1';
											
						
					end case;
					
				end if;
				
				if AvMM_Write = '1' then
				
					case AvMM_Addr(9 downto 2) is
					
						when x"00"	=>	reset_gbtfpga          <= AvMM_WriteData(0);					
						when others => NULL;
						
					end case;
					
				end if;
				
			 end if;
			 
	end process registers;  
		
end structural;